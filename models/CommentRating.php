<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commentRating".
 *
 * @property int $id
 * @property int $comment_id
 * @property int $rated_by_user_id
 * @property int $value
 */
class CommentRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commentrating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment_id', 'rated_by_user_id', 'value'], 'required'],
            [['comment_id', 'rated_by_user_id', 'value'], 'default', 'value' => null],
            [['comment_id', 'rated_by_user_id', 'value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment_id' => 'Comment ID',
            'rated_by_user_id' => 'Rated By User ID',
            'value' => 'Value',
        ];
    }
}
