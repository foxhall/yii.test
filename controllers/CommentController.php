<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use app\models\CommentRating;
use yii\filters\AccessControl;

class CommentController extends \yii\web\Controller
{
  
  public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['create', 'rate'],
              'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
              ],
          ],
      ];
  }

  public function actionCreate()
  {
      $model = new Comment();

      if($model->load(Yii::$app->request->post())) {
        $model->user_id = Yii::$app->user->getId();
        if($model->save()) {
          return 'Comment saved!';
        } else {
          return 'validation failed';
        }
      }
  }

  public function actionRate()
  {
    if(Yii::$app->request->isAjax) {
      $comment_id = (int)Yii::$app->request->post('comment_id');
      $user_id = Yii::$app->user->getId();

      $comment = Comment::find()->where(['=', 'id', $comment_id])->one();
      if($comment->user_id != $user_id) {
        $model = new CommentRating();
        $model->rated_by_user_id = $user_id;
        $model->value = (int)Yii::$app->request->post('value');
        $model->comment_id = $comment_id;
        if($model->save()) {
            $comment->average_rating = $this->getAverageRating($comment_id);
            $comment->save();
            return 'Rating saved!';
        }
      } else {
        return 'Cannot rate your own comments';
      }
    }
  }
  
  private function getAverageRating($comment_id)
	{
        $entries = CommentRating::find()->where(['=', 'comment_id', $comment_id]);
        $count = $entries->count();
        if($count != 0) {
            $sum = 0;
            foreach ($entries->all() as $entry) {
                $sum += $entry->value;
            }
            $averageRating = $sum / $count;
        } else {
            $averageRating = 0;
        }

		return $averageRating;
	}
}
