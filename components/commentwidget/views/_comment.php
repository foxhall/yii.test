<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

?>

  <?php if (!Yii::$app->user->isGuest) : ?>
    <?php echo $this->render('_form', [
        'model' => $model
    ]); ?>
  <?php endif; ?>

  <div class="form-group">
    <label for="sel1">Сортировка:</label>
    <select class="form-control" onchange="sort(this)">
      <option value="1">Сначала старые</option>
      <option value="2">Сначала новые</option>
      <option value="3">Рейтинг (по возрастанию)</option>
      <option value="4">Рейтинг (по убыванию)</option>
    </select>
  </div>

<?php Pjax::begin(['id' => 'pjax-container']);?>
  <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'itemView' => '_list_item',
    ]);
  ?>
<?php Pjax::end();?>