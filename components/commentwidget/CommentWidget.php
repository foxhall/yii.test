<?php

namespace app\components\commentwidget;

use yii\base\Widget;
use app\models\Comment;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

class CommentWidget extends Widget {

	public function init() {
		CommentWidgetAsset::register( $this->getView() );
		parent::init();

	}

	public function run() {
		$model = new Comment();
		$sort = new Sort([
			'attributes' => [
					'id',
					'average_rating'
			],
			'defaultOrder' => ['id'=>SORT_ASC]
	]);
		$dataProvider = new ActiveDataProvider([
			'query' => Comment::find()->where(['parent_id' => null]),
			'sort' => $sort,
			'pagination' => [
					'pageSize' => 20,
			],
		]);
		return $this->render('_comment', [
			'model' => $model,
			'dataProvider' => $dataProvider,
		]);
	}

}