<?php

use yii\helpers\Html;

?>
<div class="comments-list">
  <div class="comment-box">
    <div class="comment-head">
      <div class="comment-name by-author">ID пользователя: <?= Html::encode($model->user_id) ?></div>
      <span class="date"><?= Html::encode($model->created_at) ?></span>
      <span style="margin-right: 10px;"><?= ($model->average_rating) ? 'Рейтинг: ' . $model->average_rating : '' ?></span>
      <?php if (!Yii::$app->user->isGuest && $model->enabledToRate() && $model->user_id != Yii::$app->user->getId()) : ?>
        <div class="rating" comment-id="<?= $model->id ?>">
          <span value="5">☆</span>
          <span value="4">☆</span>
          <span value="3">☆</span>
          <span value="2">☆</span>
          <span value="1">☆</span>
        </div>
      <?php endif; ?>
      <span class="reply" onclick="reply(<?= Html::encode($model->id) ?>)">Ответить</span>
    </div>
    <div class="comment-content">
      <?= Html::encode($model->content) ?>
    </div>
  </div>
  
    <?php echo $this->render('_form', [
        'model' => $model,
        'parent_id' => true,
    ]); ?>
</div>

<?php if ($model->hasChildren()) : ?>
    <ul class="children">
        <?php foreach ($model->getChildren() as $child) : ?>
            <?php echo $this->render('_list_item', ['model' => $child]); ?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>