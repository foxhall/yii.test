<?php

use app\components\commentwidget\CommentWidget;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Тестовое задание: Комментарии</h2>
        <p class="lead">Чтобы оставлять комментарии необходимо войти в аккаунт.</p>
        <p class="lead">Аккаунт 1: admin / admin. Аккаунт 2: demo / demo.</p>
        <p class="lead">Оценивать комментарии можно лишь в том случае, если эти комментарии не принадлежат тому же аккаунту.</p>
    </div>

    <?= CommentWidget::widget() ?>
</div>
