function sendComment() {
	$.ajax({
		url: '/site/send-comment',
		type: 'post',
		data: {
			content: document.querySelector('#comment').value
		},
		success: function(response) {
			console.log(response);
		},
		error: function(response) {
			console.log(response);
		}
	});
}

$(document).on('beforeSubmit', function(e) {
	var formId = '#' + e.target.id;
	var form = $(formId);

	$.ajax({
		url: form.attr('action'),
		type: 'post',
		data: form.serialize(),
		success: function(response) {
			alert(response);
			form.trigger('reset');
			$.pjax.reload({ container: '#pjax-container' });
		},
		error: function(response) {
			console.log(response);
		}
	});

	return false;
});

function reply(commentId) {
	document.querySelector('#reply-to-' + commentId).classList.remove('hidden');
}

function sort(element) {
	if (element.value == 1) {
		$.pjax.reload({ container: '#pjax-container', url: '?sort=id' });
	} else if (element.value == 2) {
		$.pjax.reload({ container: '#pjax-container', url: '?sort=-id' });
	} else if (element.value == 3) {
		$.pjax.reload({ container: '#pjax-container', url: '?sort=average_rating' });
	} else {
		$.pjax.reload({ container: '#pjax-container', url: '?sort=-average_rating' });
	}
}
function init() {
	$('.rating').click(function(e) {
		if (e.target.tagName === 'SPAN') {
			console.log(e.target.getAttribute('value'));
			$.ajax({
				url: 'comment/rate',
				type: 'post',
				data: {
					comment_id: e.target.parentNode.getAttribute('comment-id'),
					value: e.target.getAttribute('value')
				},
				success: function(response) {
					alert(response);
					$.pjax.reload({ container: '#pjax-container' });
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
	});
}
$(document).ready(function() {
	init();
});
$(document).on('pjax:complete', function() {
	init();
});
