<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

//Если форма является формой ответа к комментарию, то...
if($parent_id) {
    $model->content = "Пользователь с id {$model->user_id}, ";
    $model->parent_id = $model->id;
    $class = 'hidden';
    $formId = "reply-to-{$model->id}";
} else {
    $formId = "comment-form";
    $class = 'not-hidden';
}
?>
<div class="comment-form-wrapper">
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'options' => [
            'class' => $class
            ],
        'action' => Url::to(['/comment/create']),
    ]); ?>

    <?php echo $form->field($model, 'content')->textarea(['placeholder' => 'Введите комментарий', 'rows' => 5]); ?>
    <?php echo $form->field($model, 'parent_id')->hiddenInput()->label(false); ?>
    
    <div class="form-group clearfix">
        <?php echo Html::submitButton('Отправить', ['class' => 'btn btn-default sendComment']); ?>
    </div>
    <hr>
    <?php $form->end(); ?>
</div>