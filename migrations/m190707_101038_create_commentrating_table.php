<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%commentrating}}`.
 */
class m190707_101038_create_commentrating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%commentrating}}', [
            'id' => $this->primaryKey(),
            'comment_id' => $this->integer()->notNull(),
            'rated_by_user_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%commentrating}}');
    }
}
