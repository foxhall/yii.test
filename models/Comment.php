<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\CommentRating;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $content
 * @property int $parent_id
 * @property int $user_id
 * @property int $average_rating
 * @property string $created_at
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'user_id'], 'required'],
            [['parent_id', 'user_id', 'average_rating'], 'default', 'value' => null],
            [['parent_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Комментарий',
            'parent_id' => 'Parent ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'average_rating' => 'Created At',
        ];
    }
    
    public function hasChildren()
	{
        $children = Comment::find()->where(['=', 'parent_id', $this->id])->all();
		return ($children) ? true : false;
	}
    public function getChildren()
	{
        $children = Comment::find()->where(['=', 'parent_id', $this->id])->all();
		return $children;
	}
    public function getParent()
	{
        $parent = Comment::find()->where(['=', 'id', $this->parent_id])->one();
		return $parent;
	}
    public function enabledToRate()
	{
        if(Yii::$app->user->isGuest) {
            $value = false;
        } else {
            $entries = CommentRating::find()->where(['=', 'comment_id', $this->id])->andWhere(['=', 'rated_by_user_id', Yii::$app->user->getId()])->one();
            $value = ($entries) ? false : true;
        }
        return $value;
	}
}
