<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\commentwidget;

use yii\web\AssetBundle;

class CommentWidgetAsset extends AssetBundle
{
    public $sourcePath = '@app/components/commentwidget/assets';
    public $css = [
        'css/commentwidget.css',
    ];
    public $js = [
        'js/commentwidget.js',
    ];
    
	public $depends = [
    	'yii\web\JqueryAsset'
	];
}
